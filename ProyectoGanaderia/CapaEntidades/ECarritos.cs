﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class ECarritos
    {
        public string id { get; set; }
        public string idanimal { get; set; }
        public string finca { get; set; }

        public ECarritos()
        {
        }

        public ECarritos(string id, string idanimal, string finca)
        {
            this.id = id;
            this.idanimal = idanimal;
            this.finca = finca;
        }
    }
}
