﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public abstract class EAnimal
    {
        public string edad { get; set; }
        public string sexo { get; set; }
        public string nombre { get; set; }

        protected EAnimal()
        {
        }

        protected EAnimal(string edad, string sexo, string nombre)
        {
            this.edad = edad;
            this.sexo = sexo;
            this.nombre = nombre;
        }
    }
}
