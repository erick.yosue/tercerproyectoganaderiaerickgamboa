﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public abstract class EPersona
    {
        public string Nombre { get; set; }
        public string Cedula { get; set; }

        protected EPersona()
        {
        }

        protected EPersona(string nombre, string cedula)
        {
            Nombre = nombre;
            Cedula = cedula;
        }
    }
}
