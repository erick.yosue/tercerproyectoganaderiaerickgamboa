﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class ESolicitudRevision: EBovino
    {
        public string descripcion { get; set; }
        public string idAnimal { get; set; }

        public ESolicitudRevision() : base()
        {
        }

        public ESolicitudRevision(int id, string idAnimal, string nombre, string edad, string sexo, string peso, string raza, string precio, string descripcion) : base(id, nombre, edad, sexo,  peso, raza, precio)
        {
            this.descripcion = descripcion;
            this.idAnimal = idAnimal;
        }
    }
}
