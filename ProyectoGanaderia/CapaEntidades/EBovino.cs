﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class EBovino
    {
        public int id { get; set; }
        public string edad { get; set; }
        public string sexo { get; set; }
        public string nombre { get; set; }
        public string peso { get; set; }
        public string raza { get; set; }
        public string precio { get; set; }

        public EBovino()
        {
        }

        public EBovino(int id, string edad, string sexo, string nombre, string peso, string raza, string precio)
        {
            this.id = id;
            this.edad = edad;
            this.sexo = sexo;
            this.nombre = nombre;
            this.peso = peso;
            this.raza = raza;
            this.precio = precio;
        }
    }

}
