﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
   public class EUsuario : EPersona
    {
        public int id { get; set; }
        public string Contrasenna { get; set; }
        public string tipo { get; set; }

        public EUsuario() : base()
        {
        }

        public EUsuario(int id, string contrasenna, string tipo, string nombre, string cedula) : base (nombre,cedula)
        {
            this.id = id;
            Contrasenna = contrasenna;
            this.tipo = tipo;
        }
    }
}
