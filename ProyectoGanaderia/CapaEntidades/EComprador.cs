﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
   public class EComprador
    {
        public int id { get; set; }
        public string peso { get; set; }
        public string raza { get; set; }
        public string precio { get; set; }
        public string edad { get; set; }
        public string sexo { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string idanimal { get; set; }

        public EComprador()
        {
        }

        public EComprador(int id, string peso, string raza, string precio, string edad, string sexo, string nombre, string descripcion, string idanimal)
        {
            this.id = id;
            this.peso = peso;
            this.raza = raza;
            this.precio = precio;
            this.edad = edad;
            this.sexo = sexo;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.idanimal = idanimal;
        }
    }
}
