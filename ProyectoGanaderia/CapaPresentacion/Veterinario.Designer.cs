﻿namespace CapaPresentacion
{
    partial class Veterinario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Veterinario));
            this.dtgvVeterinario = new System.Windows.Forms.DataGridView();
            this.userControlMensajeError1 = new Componentes.UserControlMensajeError();
            this.userControlHeader1 = new Componentes.UserControlHeader();
            this.txtDescripVaquero = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSexo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.txtRaza = new System.Windows.Forms.TextBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.vtnSeleccionar = new System.Windows.Forms.Button();
            this.idSeleccionado = new System.Windows.Forms.Label();
            this.mensajeSeleccion = new System.Windows.Forms.Label();
            this.idSoli = new System.Windows.Forms.Label();
            this.mensajeSeleSoli = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.mensajeVete = new System.Windows.Forms.Label();
            this.txtCostorecuperacion = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVeterinario)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvVeterinario
            // 
            this.dtgvVeterinario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvVeterinario.Location = new System.Drawing.Point(253, 158);
            this.dtgvVeterinario.Name = "dtgvVeterinario";
            this.dtgvVeterinario.Size = new System.Drawing.Size(460, 251);
            this.dtgvVeterinario.TabIndex = 8;
            this.dtgvVeterinario.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvVeterinario_CellContentClick);
            // 
            // userControlMensajeError1
            // 
            this.userControlMensajeError1.Location = new System.Drawing.Point(0, 385);
            this.userControlMensajeError1.Name = "userControlMensajeError1";
            this.userControlMensajeError1.Size = new System.Drawing.Size(713, 146);
            this.userControlMensajeError1.TabIndex = 7;
            this.userControlMensajeError1.Load += new System.EventHandler(this.userControlMensajeError1_Load);
            // 
            // userControlHeader1
            // 
            this.userControlHeader1.Location = new System.Drawing.Point(0, -4);
            this.userControlHeader1.Name = "userControlHeader1";
            this.userControlHeader1.Size = new System.Drawing.Size(713, 187);
            this.userControlHeader1.TabIndex = 6;
            // 
            // txtDescripVaquero
            // 
            this.txtDescripVaquero.Location = new System.Drawing.Point(27, 394);
            this.txtDescripVaquero.Name = "txtDescripVaquero";
            this.txtDescripVaquero.Size = new System.Drawing.Size(192, 64);
            this.txtDescripVaquero.TabIndex = 41;
            this.txtDescripVaquero.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Descripción :";
            // 
            // txtSexo
            // 
            this.txtSexo.FormattingEnabled = true;
            this.txtSexo.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.txtSexo.Location = new System.Drawing.Point(119, 358);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(100, 21);
            this.txtSexo.TabIndex = 39;
            this.txtSexo.Text = "Sexo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 38;
            this.label6.Text = "Precio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 37;
            this.label5.Text = "Raza";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 298);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 15);
            this.label4.TabIndex = 36;
            this.label4.Text = "Edad";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 329);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 35;
            this.label2.Text = "Nombre";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(24, 194);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(35, 15);
            this.lbl.TabIndex = 34;
            this.lbl.Tag = "lbll";
            this.lbl.Text = "Peso";
            // 
            // txtRaza
            // 
            this.txtRaza.Location = new System.Drawing.Point(119, 220);
            this.txtRaza.Name = "txtRaza";
            this.txtRaza.Size = new System.Drawing.Size(100, 20);
            this.txtRaza.TabIndex = 33;
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(119, 255);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(100, 20);
            this.txtPrecio.TabIndex = 32;
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(119, 293);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 20);
            this.txtEdad.TabIndex = 31;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(119, 324);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 30;
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(119, 189);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(100, 20);
            this.txtPeso.TabIndex = 29;
            // 
            // vtnSeleccionar
            // 
            this.vtnSeleccionar.Image = ((System.Drawing.Image)(resources.GetObject("vtnSeleccionar.Image")));
            this.vtnSeleccionar.Location = new System.Drawing.Point(328, 415);
            this.vtnSeleccionar.Name = "vtnSeleccionar";
            this.vtnSeleccionar.Size = new System.Drawing.Size(64, 50);
            this.vtnSeleccionar.TabIndex = 42;
            this.vtnSeleccionar.UseVisualStyleBackColor = true;
            this.vtnSeleccionar.Click += new System.EventHandler(this.vtnSeleccionar_Click);
            // 
            // idSeleccionado
            // 
            this.idSeleccionado.AutoSize = true;
            this.idSeleccionado.Location = new System.Drawing.Point(186, 158);
            this.idSeleccionado.Name = "idSeleccionado";
            this.idSeleccionado.Size = new System.Drawing.Size(0, 13);
            this.idSeleccionado.TabIndex = 44;
            // 
            // mensajeSeleccion
            // 
            this.mensajeSeleccion.AutoSize = true;
            this.mensajeSeleccion.Location = new System.Drawing.Point(27, 158);
            this.mensajeSeleccion.Name = "mensajeSeleccion";
            this.mensajeSeleccion.Size = new System.Drawing.Size(0, 13);
            this.mensajeSeleccion.TabIndex = 43;
            // 
            // idSoli
            // 
            this.idSoli.AutoSize = true;
            this.idSoli.Location = new System.Drawing.Point(186, 119);
            this.idSoli.Name = "idSoli";
            this.idSoli.Size = new System.Drawing.Size(0, 13);
            this.idSoli.TabIndex = 46;
            // 
            // mensajeSeleSoli
            // 
            this.mensajeSeleSoli.AutoSize = true;
            this.mensajeSeleSoli.Location = new System.Drawing.Point(27, 119);
            this.mensajeSeleSoli.Name = "mensajeSeleSoli";
            this.mensajeSeleSoli.Size = new System.Drawing.Size(0, 13);
            this.mensajeSeleSoli.TabIndex = 45;
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(581, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 50);
            this.button1.TabIndex = 47;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(496, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 50);
            this.button2.TabIndex = 48;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(410, 415);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 50);
            this.button3.TabIndex = 49;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // mensajeVete
            // 
            this.mensajeVete.AutoSize = true;
            this.mensajeVete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mensajeVete.Location = new System.Drawing.Point(235, 495);
            this.mensajeVete.Name = "mensajeVete";
            this.mensajeVete.Size = new System.Drawing.Size(0, 16);
            this.mensajeVete.TabIndex = 50;
            // 
            // txtCostorecuperacion
            // 
            this.txtCostorecuperacion.Location = new System.Drawing.Point(477, 491);
            this.txtCostorecuperacion.Name = "txtCostorecuperacion";
            this.txtCostorecuperacion.Size = new System.Drawing.Size(100, 20);
            this.txtCostorecuperacion.TabIndex = 51;
            this.txtCostorecuperacion.TextChanged += new System.EventHandler(this.txtCostorecuperacion_TextChanged);
            this.txtCostorecuperacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCostorecuperacion_KeyPress);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(657, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 34);
            this.button4.TabIndex = 52;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Veterinario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 529);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtCostorecuperacion);
            this.Controls.Add(this.mensajeVete);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.idSoli);
            this.Controls.Add(this.mensajeSeleSoli);
            this.Controls.Add(this.idSeleccionado);
            this.Controls.Add(this.mensajeSeleccion);
            this.Controls.Add(this.vtnSeleccionar);
            this.Controls.Add(this.txtDescripVaquero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.txtRaza);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.dtgvVeterinario);
            this.Controls.Add(this.userControlMensajeError1);
            this.Controls.Add(this.userControlHeader1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Veterinario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Veterinario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVeterinario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvVeterinario;
        private Componentes.UserControlMensajeError userControlMensajeError1;
        private Componentes.UserControlHeader userControlHeader1;
        private System.Windows.Forms.RichTextBox txtDescripVaquero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox txtSexo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtRaza;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Button vtnSeleccionar;
        private System.Windows.Forms.Label idSeleccionado;
        private System.Windows.Forms.Label mensajeSeleccion;
        private System.Windows.Forms.Label idSoli;
        private System.Windows.Forms.Label mensajeSeleSoli;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label mensajeVete;
        private System.Windows.Forms.TextBox txtCostorecuperacion;
        private System.Windows.Forms.Button button4;
    }
}