﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;

namespace CapaPresentacion
{
    public partial class Veterinario : Form
    {
        public Veterinario()
        {
            InitializeComponent();
        }
        LVeterinario veterinario = new LVeterinario();
        LVaquero vaquero = new LVaquero();

        /*
         Este método es el encargado de que el usuario solo pueda escribir números cuando se invoque el método
             */

        public void SoloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }

        /*
         Este método es el encargado de limpiar los componentes que se muestran en la ventana 
             */

        public void limpiarDatos()
        {
            txtEdad.Text = "";
            txtNombre.Text = "";
            txtPeso.Text = "";
            txtPrecio.Text = "";
            txtRaza.Text = "";
            txtSexo.Text = "";
            mensajeSeleccion.Text = "";
            idSeleccionado.Text = "";
            txtDescripVaquero.Text = "";
            mensajeSeleSoli.Text = ""; ;
            idSoli.Text = "";

        }

        private void dtgvVeterinario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Veterinario_Load(object sender, EventArgs e)
        {
            dtgvVeterinario.DataSource = null;
            dtgvVeterinario.DataSource = veterinario.CargarSolicitud("");
            txtCostorecuperacion.Visible = false;
        }

        private void vtnSeleccionar_Click(object sender, EventArgs e)
        {
            mensajeSeleSoli.Text = "Solicitud seleccionada:"; ;
            idSoli.Text = dtgvVeterinario.CurrentRow.Cells[2].Value.ToString();
            mensajeSeleccion.Text = "Bovino seleccionado:";
            idSeleccionado.Text = dtgvVeterinario.CurrentRow.Cells[1].Value.ToString();
            txtPeso.Text = dtgvVeterinario.CurrentRow.Cells[6].Value.ToString();
            txtRaza.Text = dtgvVeterinario.CurrentRow.Cells[7].Value.ToString();
            txtPrecio.Text = dtgvVeterinario.CurrentRow.Cells[8].Value.ToString();
            txtEdad.Text = dtgvVeterinario.CurrentRow.Cells[3].Value.ToString();
            txtSexo.Text = dtgvVeterinario.CurrentRow.Cells[4].Value.ToString();
            txtNombre.Text = dtgvVeterinario.CurrentRow.Cells[5].Value.ToString();
            txtDescripVaquero.Text = dtgvVeterinario.CurrentRow.Cells[0].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (idSoli.Text != "")
            {
                veterinario.EliminarSolicitud(Convert.ToInt32(dtgvVeterinario.CurrentRow.Cells[2].Value.ToString()));
                dtgvVeterinario.DataSource = null;
                dtgvVeterinario.DataSource = veterinario.CargarSolicitud("");
                limpiarDatos();
                mensajeVete.Text = "¡Solicitud rechazada!";
            }
            else
            {
                mensajeVete.Text = "¡Debe seleccionar la solicitud!";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (idSoli.Text != "")
            {
                mensajeVete.Text = "¡Seleccione el costo de tratamiento!";
                txtCostorecuperacion.Visible = true;
                if (txtCostorecuperacion.Text != "")
                {
                    mensajeVete.Text = "¡Animal en tratamiento!";
                    veterinario.EliminarSolicitud(Convert.ToInt32(dtgvVeterinario.CurrentRow.Cells[2].Value.ToString()));
                    dtgvVeterinario.DataSource = null;
                    dtgvVeterinario.DataSource = veterinario.CargarSolicitud("");
                    limpiarDatos();
                    txtCostorecuperacion.Visible = false;
                }
            }
            else
            {
                mensajeVete.Text = "¡Debe seleccionar la solicitud!";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (idSoli.Text != "")
            {
                mensajeVete.Text = "¡Seleccione el descuento del animal!";
                txtCostorecuperacion.Visible = true;
                if (txtCostorecuperacion.Text != "")
                {
                    mensajeVete.Text = "¡Animal puesto en venta!";
                    veterinario.EliminarSolicitud(Convert.ToInt32(dtgvVeterinario.CurrentRow.Cells[2].Value.ToString()));
                    dtgvVeterinario.DataSource = null;
                    dtgvVeterinario.DataSource = veterinario.CargarSolicitud("");
                    vaquero.VentaBovino(txtPeso.Text, txtRaza.Text, txtPrecio.Text, txtEdad.Text, txtSexo.Text, txtNombre.Text, txtDescripVaquero.Text, idSeleccionado.Text);
                    limpiarDatos();
                    txtCostorecuperacion.Visible = false;



                }
            }
            else
            {
                mensajeVete.Text = "¡Debe seleccionar la solicitud!";
            }
        }

        private void txtCostorecuperacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtCostorecuperacion_TextChanged(object sender, EventArgs e)
        {

        }

        private void userControlMensajeError1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Inicio inicio = new Inicio();
            inicio.Show(this);
            this.Hide();
        }
    }
}
