﻿namespace CapaPresentacion
{
    partial class Vaquero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vaquero));
            this.dtgvVaquero = new System.Windows.Forms.DataGridView();
            this.userControlMensajeError1 = new Componentes.UserControlMensajeError();
            this.userControlHeader1 = new Componentes.UserControlHeader();
            this.txtSexo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.txtRaza = new System.Windows.Forms.TextBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtDescripVaquero = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.vtnSeleccionar = new System.Windows.Forms.Button();
            this.idSeleccionado = new System.Windows.Forms.Label();
            this.mensajeSeleccion = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblMensajeVaque = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVaquero)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvVaquero
            // 
            this.dtgvVaquero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvVaquero.Location = new System.Drawing.Point(253, 158);
            this.dtgvVaquero.Name = "dtgvVaquero";
            this.dtgvVaquero.Size = new System.Drawing.Size(460, 251);
            this.dtgvVaquero.TabIndex = 5;
            // 
            // userControlMensajeError1
            // 
            this.userControlMensajeError1.Location = new System.Drawing.Point(0, 385);
            this.userControlMensajeError1.Name = "userControlMensajeError1";
            this.userControlMensajeError1.Size = new System.Drawing.Size(713, 146);
            this.userControlMensajeError1.TabIndex = 4;
            // 
            // userControlHeader1
            // 
            this.userControlHeader1.Location = new System.Drawing.Point(0, -4);
            this.userControlHeader1.Name = "userControlHeader1";
            this.userControlHeader1.Size = new System.Drawing.Size(713, 187);
            this.userControlHeader1.TabIndex = 3;
            // 
            // txtSexo
            // 
            this.txtSexo.FormattingEnabled = true;
            this.txtSexo.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.txtSexo.Location = new System.Drawing.Point(121, 358);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(100, 21);
            this.txtSexo.TabIndex = 26;
            this.txtSexo.Text = "Sexo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 25;
            this.label6.Text = "Precio";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 24;
            this.label5.Text = "Raza";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 298);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 15);
            this.label4.TabIndex = 23;
            this.label4.Text = "Edad";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 329);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 22;
            this.label2.Text = "Nombre";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(26, 194);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(35, 15);
            this.lbl.TabIndex = 21;
            this.lbl.Tag = "lbll";
            this.lbl.Text = "Peso";
            this.lbl.Click += new System.EventHandler(this.lbl_Click);
            // 
            // txtRaza
            // 
            this.txtRaza.Location = new System.Drawing.Point(121, 220);
            this.txtRaza.Name = "txtRaza";
            this.txtRaza.Size = new System.Drawing.Size(100, 20);
            this.txtRaza.TabIndex = 20;
            this.txtRaza.TextChanged += new System.EventHandler(this.txtRaza_TextChanged);
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(121, 255);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(100, 20);
            this.txtPrecio.TabIndex = 19;
            this.txtPrecio.TextChanged += new System.EventHandler(this.txtPrecio_TextChanged);
            // 
            // txtEdad
            // 
            this.txtEdad.Location = new System.Drawing.Point(121, 293);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 20);
            this.txtEdad.TabIndex = 18;
            this.txtEdad.TextChanged += new System.EventHandler(this.txtEdad_TextChanged);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(121, 324);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 17;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(121, 189);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(100, 20);
            this.txtPeso.TabIndex = 16;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            // 
            // txtDescripVaquero
            // 
            this.txtDescripVaquero.Location = new System.Drawing.Point(29, 394);
            this.txtDescripVaquero.Name = "txtDescripVaquero";
            this.txtDescripVaquero.Size = new System.Drawing.Size(192, 64);
            this.txtDescripVaquero.TabIndex = 28;
            this.txtDescripVaquero.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Descripción :";
            // 
            // vtnSeleccionar
            // 
            this.vtnSeleccionar.Image = ((System.Drawing.Image)(resources.GetObject("vtnSeleccionar.Image")));
            this.vtnSeleccionar.Location = new System.Drawing.Point(280, 416);
            this.vtnSeleccionar.Name = "vtnSeleccionar";
            this.vtnSeleccionar.Size = new System.Drawing.Size(64, 50);
            this.vtnSeleccionar.TabIndex = 29;
            this.vtnSeleccionar.UseVisualStyleBackColor = true;
            this.vtnSeleccionar.Click += new System.EventHandler(this.vtnSeleccionar_Click);
            // 
            // idSeleccionado
            // 
            this.idSeleccionado.AutoSize = true;
            this.idSeleccionado.Location = new System.Drawing.Point(191, 158);
            this.idSeleccionado.Name = "idSeleccionado";
            this.idSeleccionado.Size = new System.Drawing.Size(0, 13);
            this.idSeleccionado.TabIndex = 31;
            // 
            // mensajeSeleccion
            // 
            this.mensajeSeleccion.AutoSize = true;
            this.mensajeSeleccion.Location = new System.Drawing.Point(32, 158);
            this.mensajeSeleccion.Name = "mensajeSeleccion";
            this.mensajeSeleccion.Size = new System.Drawing.Size(0, 13);
            this.mensajeSeleccion.TabIndex = 30;
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(394, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 50);
            this.button1.TabIndex = 32;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblMensajeVaque
            // 
            this.lblMensajeVaque.AutoSize = true;
            this.lblMensajeVaque.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensajeVaque.Location = new System.Drawing.Point(250, 504);
            this.lblMensajeVaque.Name = "lblMensajeVaque";
            this.lblMensajeVaque.Size = new System.Drawing.Size(0, 16);
            this.lblMensajeVaque.TabIndex = 33;
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(508, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 50);
            this.button2.TabIndex = 34;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(578, 416);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(26, 27);
            this.btnGuardar.TabIndex = 35;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(625, 415);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 49);
            this.button3.TabIndex = 36;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(657, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 34);
            this.button4.TabIndex = 37;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Vaquero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 529);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblMensajeVaque);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.idSeleccionado);
            this.Controls.Add(this.mensajeSeleccion);
            this.Controls.Add(this.vtnSeleccionar);
            this.Controls.Add(this.txtDescripVaquero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.txtRaza);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.txtEdad);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.dtgvVaquero);
            this.Controls.Add(this.userControlMensajeError1);
            this.Controls.Add(this.userControlHeader1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Vaquero";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Vaquero_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvVaquero)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvVaquero;
        private Componentes.UserControlMensajeError userControlMensajeError1;
        private Componentes.UserControlHeader userControlHeader1;
        private System.Windows.Forms.ComboBox txtSexo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtRaza;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.RichTextBox txtDescripVaquero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button vtnSeleccionar;
        private System.Windows.Forms.Label idSeleccionado;
        private System.Windows.Forms.Label mensajeSeleccion;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblMensajeVaque;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}