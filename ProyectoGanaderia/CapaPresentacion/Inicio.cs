﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {
            lblTituloPrincipal.Parent = PicBoxInicio;
            lblTituloPrincipal.BackColor = Color.Transparent;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show(this);
            this.Hide();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Registrar registrar = new Registrar();
            registrar.Show(this);
            this.Hide();
        }

        private void PicBoxInicio_Click(object sender, EventArgs e)
        {

        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            Comprador comprador = new Comprador();
            comprador.Show(this);
            this.Hide();
        }
    }
}
