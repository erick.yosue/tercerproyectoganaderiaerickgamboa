﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;

namespace CapaPresentacion
{
    public partial class Comprador : Form
    {
        public Comprador()
        {
            InitializeComponent();
        }

        public void limpiarDatos()
        /*
             Este método es el encargado de limpiar los componentes que se muestran en la ventana 
         */
        {
            txtfinc.Text = "";
            txtdire.Text = "";
            animalSelec.Text = "";
            idAnimalselect.Text = "";
            idVenta.Text = "";
        }

        LComprador comprador = new LComprador();

        private void Comprador_Load(object sender, EventArgs e)
        {
            txtfinc.Visible = false;
            txtdire.Visible = false;
            dtgvComprador.DataSource = null;
            dtgvComprador.DataSource = comprador.CargarAnimalesVenta("");

        }

        private void vtnSeleccionar_Click(object sender, EventArgs e)
        {
            animalSelec.Text = "El animal seleccionado es: ";
            idAnimalselect.Text = dtgvComprador.CurrentRow.Cells[8].Value.ToString(); ;
            idVenta.Text = dtgvComprador.CurrentRow.Cells[0].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtfinc.Visible = true;
            txtdire.Visible = true;
            fincaa.Text = "Finca: ";
            direccion.Text = "Dirección: ";
            if (txtfinc.Text != "" && txtdire.Text != "" && idAnimalselect.Text != "")
            {
                comprador.ComprarAnimal(idAnimalselect.Text, txtfinc.Text);
                comprados.DataSource = null;
                comprados.DataSource = comprador.CargarAnimalescomprados("");
                comprador.QuitarAnimalVenta(Convert.ToInt32(idVenta.Text));
                limpiarDatos();
                dtgvComprador.DataSource = null;
                dtgvComprador.DataSource = comprador.CargarAnimalesVenta("");

            }
            else
            {
                mensajecompra.Text = "Debe completar la información";
            }

        }

        private void fincaa_Click(object sender, EventArgs e)
        {

        }
    }
}
