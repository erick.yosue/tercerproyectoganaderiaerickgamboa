﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;

namespace CapaPresentacion
{
    public partial class Registrar : Form
    {
        public Registrar()
        {
            InitializeComponent();
        }

        LUsuario usuario = new LUsuario();

        /*
         Este método es el encargado de que el usuario solo pueda escribir números cuando se invoque el método
             */

        public void soloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }

        /*
         Este método es el encargado de limpiar los componentes que se muestran en la ventana 
             */

        private void limpiarVentanaReg()
        {
            txtNombre.Text = "";
            txtcontra.Text = "";
            txtCedula.Text = "";

        }
        private void button2_Click(object sender, EventArgs e)
        {
            Inicio inicio = new Inicio();
            inicio.Show(this);
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtcontra.Text != "" && txtTipoUsu.Text != "" && txtCedula.Text != "")
            {
                usuario.registrarUsuarios(txtNombre.Text, txtcontra.Text, txtCedula.Text, txtTipoUsu.Text);
                lblMensajeRegistrar.Text = "Registro exitoso";
                limpiarVentanaReg();
            }
            else
            {
                lblMensajeRegistrar.Text = "Debe llenar correctamente todos los campos";
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
