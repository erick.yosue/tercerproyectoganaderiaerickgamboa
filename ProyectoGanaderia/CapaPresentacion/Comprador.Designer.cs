﻿namespace CapaPresentacion
{
    partial class Comprador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Comprador));
            this.dtgvComprador = new System.Windows.Forms.DataGridView();
            this.userControlMensajeError1 = new Componentes.UserControlMensajeError();
            this.userControlHeader1 = new Componentes.UserControlHeader();
            this.comprados = new System.Windows.Forms.DataGridView();
            this.animalSelec = new System.Windows.Forms.Label();
            this.idAnimalselect = new System.Windows.Forms.Label();
            this.vtnSeleccionar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtfinc = new System.Windows.Forms.TextBox();
            this.txtdire = new System.Windows.Forms.TextBox();
            this.fincaa = new System.Windows.Forms.Label();
            this.direccion = new System.Windows.Forms.Label();
            this.mensajecompra = new System.Windows.Forms.Label();
            this.idVenta = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvComprador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comprados)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvComprador
            // 
            this.dtgvComprador.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvComprador.Location = new System.Drawing.Point(253, 159);
            this.dtgvComprador.Name = "dtgvComprador";
            this.dtgvComprador.Size = new System.Drawing.Size(460, 251);
            this.dtgvComprador.TabIndex = 11;
            // 
            // userControlMensajeError1
            // 
            this.userControlMensajeError1.Location = new System.Drawing.Point(0, 389);
            this.userControlMensajeError1.Name = "userControlMensajeError1";
            this.userControlMensajeError1.Size = new System.Drawing.Size(713, 146);
            this.userControlMensajeError1.TabIndex = 10;
            // 
            // userControlHeader1
            // 
            this.userControlHeader1.Location = new System.Drawing.Point(0, -3);
            this.userControlHeader1.Name = "userControlHeader1";
            this.userControlHeader1.Size = new System.Drawing.Size(713, 187);
            this.userControlHeader1.TabIndex = 9;
            // 
            // comprados
            // 
            this.comprados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.comprados.Location = new System.Drawing.Point(0, 159);
            this.comprados.Name = "comprados";
            this.comprados.Size = new System.Drawing.Size(254, 251);
            this.comprados.TabIndex = 12;
            // 
            // animalSelec
            // 
            this.animalSelec.AutoSize = true;
            this.animalSelec.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.animalSelec.Location = new System.Drawing.Point(183, 132);
            this.animalSelec.Name = "animalSelec";
            this.animalSelec.Size = new System.Drawing.Size(0, 18);
            this.animalSelec.TabIndex = 13;
            // 
            // idAnimalselect
            // 
            this.idAnimalselect.AutoSize = true;
            this.idAnimalselect.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idAnimalselect.Location = new System.Drawing.Point(445, 132);
            this.idAnimalselect.Name = "idAnimalselect";
            this.idAnimalselect.Size = new System.Drawing.Size(0, 18);
            this.idAnimalselect.TabIndex = 14;
            // 
            // vtnSeleccionar
            // 
            this.vtnSeleccionar.Image = ((System.Drawing.Image)(resources.GetObject("vtnSeleccionar.Image")));
            this.vtnSeleccionar.Location = new System.Drawing.Point(368, 416);
            this.vtnSeleccionar.Name = "vtnSeleccionar";
            this.vtnSeleccionar.Size = new System.Drawing.Size(64, 50);
            this.vtnSeleccionar.TabIndex = 19;
            this.vtnSeleccionar.UseVisualStyleBackColor = true;
            this.vtnSeleccionar.Click += new System.EventHandler(this.vtnSeleccionar_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(498, 416);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 50);
            this.button1.TabIndex = 20;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtfinc
            // 
            this.txtfinc.Location = new System.Drawing.Point(112, 432);
            this.txtfinc.Name = "txtfinc";
            this.txtfinc.Size = new System.Drawing.Size(117, 20);
            this.txtfinc.TabIndex = 21;
            // 
            // txtdire
            // 
            this.txtdire.Location = new System.Drawing.Point(112, 479);
            this.txtdire.Name = "txtdire";
            this.txtdire.Size = new System.Drawing.Size(279, 20);
            this.txtdire.TabIndex = 22;
            // 
            // fincaa
            // 
            this.fincaa.AutoSize = true;
            this.fincaa.Location = new System.Drawing.Point(30, 435);
            this.fincaa.Name = "fincaa";
            this.fincaa.Size = new System.Drawing.Size(0, 13);
            this.fincaa.TabIndex = 23;
            this.fincaa.Click += new System.EventHandler(this.fincaa_Click);
            // 
            // direccion
            // 
            this.direccion.AutoSize = true;
            this.direccion.Location = new System.Drawing.Point(30, 482);
            this.direccion.Name = "direccion";
            this.direccion.Size = new System.Drawing.Size(0, 13);
            this.direccion.TabIndex = 24;
            // 
            // mensajecompra
            // 
            this.mensajecompra.AutoSize = true;
            this.mensajecompra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mensajecompra.Location = new System.Drawing.Point(324, 507);
            this.mensajecompra.Name = "mensajecompra";
            this.mensajecompra.Size = new System.Drawing.Size(0, 16);
            this.mensajecompra.TabIndex = 25;
            // 
            // idVenta
            // 
            this.idVenta.AutoSize = true;
            this.idVenta.Location = new System.Drawing.Point(678, 416);
            this.idVenta.Name = "idVenta";
            this.idVenta.Size = new System.Drawing.Size(0, 13);
            this.idVenta.TabIndex = 26;
            // 
            // Comprador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 529);
            this.Controls.Add(this.idVenta);
            this.Controls.Add(this.mensajecompra);
            this.Controls.Add(this.direccion);
            this.Controls.Add(this.fincaa);
            this.Controls.Add(this.txtdire);
            this.Controls.Add(this.txtfinc);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.vtnSeleccionar);
            this.Controls.Add(this.idAnimalselect);
            this.Controls.Add(this.animalSelec);
            this.Controls.Add(this.comprados);
            this.Controls.Add(this.dtgvComprador);
            this.Controls.Add(this.userControlMensajeError1);
            this.Controls.Add(this.userControlHeader1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Comprador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Comprador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvComprador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comprados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvComprador;
        private Componentes.UserControlMensajeError userControlMensajeError1;
        private Componentes.UserControlHeader userControlHeader1;
        private System.Windows.Forms.DataGridView comprados;
        private System.Windows.Forms.Label animalSelec;
        private System.Windows.Forms.Label idAnimalselect;
        private System.Windows.Forms.Button vtnSeleccionar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtfinc;
        private System.Windows.Forms.TextBox txtdire;
        private System.Windows.Forms.Label fincaa;
        private System.Windows.Forms.Label direccion;
        private System.Windows.Forms.Label mensajecompra;
        private System.Windows.Forms.Label idVenta;
    }
}