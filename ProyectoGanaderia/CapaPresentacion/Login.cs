﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;

namespace CapaPresentacion
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        LLogin login = new LLogin();

        /*
         Este método es el encargado de que el usuario solo pueda escribir números cuando se invoque el método
             */


        public void soloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Inicio inicio = new Inicio();
            inicio.Show(this);
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (login.Login(txtUsuarioLogin.Text, txtContraLogin.Text).tipo.Equals("Capataz"))
                {
                    Capataz capataz = new Capataz();
                    capataz.Show(this);
                    this.Hide();
                }
                else if (login.Login(txtUsuarioLogin.Text, txtContraLogin.Text).tipo.Equals("Veterinario"))
                {
                    Veterinario veterinario = new Veterinario();
                    veterinario.Show(this);
                    this.Hide();
                }
                else if (login.Login(txtUsuarioLogin.Text, txtContraLogin.Text).tipo.Equals("Transportista"))
                {
                    lblMensajeLogin.Text = "Transportista";
                }
                else if (login.Login(txtUsuarioLogin.Text, txtContraLogin.Text).tipo.Equals("Proveedor"))
                {
                    lblMensajeLogin.Text = "Proveedor";
                }
                else if (login.Login(txtUsuarioLogin.Text, txtContraLogin.Text).tipo.Equals("Vaquero"))
                {
                    Vaquero vaquero = new Vaquero();
                    vaquero.Show(this);
                    this.Hide();
                }
               
            }

            catch (Exception ex)
            {
                lblMensajeLogin.Text = "Credenciales no válidos";
            }


        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


    }
}




