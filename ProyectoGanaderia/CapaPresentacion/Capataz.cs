﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;
namespace CapaPresentacion
{
    public partial class Capataz : Form, InterfazFuncionesGenerales
    {
        public Capataz()
        {
            InitializeComponent();
        }

        LBovino bovino = new LBovino();
        /*
         Este método es el encargado de limpiar los componentes que se muestran en la ventana 
             */
        public void limpiarDatos()
        {
            txtEdad.Text = "";
            txtNombre.Text = "";
            txtPeso.Text = "";
            txtPrecio.Text = "";
            txtRaza.Text = "";
            txtSexo.Text = "";
            mensajeSeleccion.Text = "";
            idSeleccionado.Text = "";

        }

        /*
         Este método es el encargado de que el usuario solo pueda escribir números cuando se invoque el método
             */

        public void soloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }

        

        private void Capataz_Load(object sender, EventArgs e)
        {
            dtgvCapataz.DataSource = null;
            dtgvCapataz.DataSource = bovino.CargarBovino("");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            bovino.RegistrarBovino(txtPeso.Text, txtRaza.Text,txtPrecio.Text,txtEdad.Text,txtSexo.Text,txtNombre.Text);
            limpiarDatos();
            dtgvCapataz.DataSource = null;
            dtgvCapataz.DataSource = bovino.CargarBovino("");
            lblMensajeCapa.Text = "¡Registro exitoso!";
        }

        private void txtPeso_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void txtEdad_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void vtnSeleccionar_Click(object sender, EventArgs e)
        {
            mensajeSeleccion.Text = "Bovino seleccionado:";
            idSeleccionado.Text = dtgvCapataz.CurrentRow.Cells[0].Value.ToString();
            txtPeso.Text = dtgvCapataz.CurrentRow.Cells[1].Value.ToString();
            txtRaza.Text = dtgvCapataz.CurrentRow.Cells[2].Value.ToString();
            txtPrecio.Text = dtgvCapataz.CurrentRow.Cells[3].Value.ToString();
            txtEdad.Text = dtgvCapataz.CurrentRow.Cells[4].Value.ToString();
            txtSexo.Text = dtgvCapataz.CurrentRow.Cells[5].Value.ToString();
            txtNombre.Text = dtgvCapataz.CurrentRow.Cells[6].Value.ToString();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (idSeleccionado.Text != "")
            {
                bovino.EditarBovino(txtPeso.Text, txtRaza.Text, txtPrecio.Text, txtEdad.Text, txtSexo.Text, txtNombre.Text, Convert.ToInt32(idSeleccionado.Text));
                limpiarDatos();
                dtgvCapataz.DataSource = null;
                dtgvCapataz.DataSource = bovino.CargarBovino("");
                lblMensajeCapa.Text = "¡Edición exitosa!";
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dtgvCapataz.CurrentRow.Cells[0].Value.ToString() != "")
            {
                bovino.EliminarBovino(Convert.ToInt32(dtgvCapataz.CurrentRow.Cells[0].Value.ToString()));
                dtgvCapataz.DataSource = null;
                dtgvCapataz.DataSource = bovino.CargarBovino("");
                lblMensajeCapa.Text = "¡Eliminación exitosa!";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Inicio inicio = new Inicio();
            inicio.Show(this);
            this.Hide();
        }
    }
}
