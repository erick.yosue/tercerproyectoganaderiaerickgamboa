﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaLogica;

namespace CapaPresentacion
{
    public partial class Vaquero : Form
    {
        public Vaquero()
        {
            InitializeComponent();
        }

        LBovino bovino = new LBovino();

        LVaquero vaquero = new LVaquero();

        /*
         Este método es el encargado de que el usuario solo pueda escribir números cuando se invoque el método
             */

        public void soloNumeros(KeyPressEventArgs e)
        {
            try
            {
                if (char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }

            }
            catch (Exception)
            {
            }


        }

        /*
         Este método es el encargado de limpiar los componentes que se muestran en la ventana 
             */

        public void limpiarDatos()
        {
            txtEdad.Text = "";
            txtNombre.Text = "";
            txtPeso.Text = "";
            txtPrecio.Text = "";
            txtRaza.Text = "";
            txtSexo.Text = "";
            mensajeSeleccion.Text = "";
            idSeleccionado.Text = "";
            txtDescripVaquero.Text = "";

        }

        private void Vaquero_Load(object sender, EventArgs e)
        {
            dtgvVaquero.DataSource = null;
            dtgvVaquero.DataSource = bovino.CargarBovino("");
        }

        private void vtnSeleccionar_Click(object sender, EventArgs e)
        {
            mensajeSeleccion.Text = "Bovino seleccionado:";
            idSeleccionado.Text = dtgvVaquero.CurrentRow.Cells[0].Value.ToString();
            txtPeso.Text = dtgvVaquero.CurrentRow.Cells[4].Value.ToString();
            txtRaza.Text = dtgvVaquero.CurrentRow.Cells[5].Value.ToString();
            txtPrecio.Text = dtgvVaquero.CurrentRow.Cells[6].Value.ToString();
            txtEdad.Text = dtgvVaquero.CurrentRow.Cells[1].Value.ToString();
            txtSexo.Text = dtgvVaquero.CurrentRow.Cells[2].Value.ToString();
            txtNombre.Text = dtgvVaquero.CurrentRow.Cells[3].Value.ToString();
            lblMensajeVaque.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (idSeleccionado.Text != "")
            {
                if (txtDescripVaquero.Text != "")
                {
                    vaquero.SolicitudRevisionBovino(txtPeso.Text, txtRaza.Text, txtPrecio.Text, txtEdad.Text, txtSexo.Text, txtNombre.Text, txtDescripVaquero.Text, idSeleccionado.Text);
                    limpiarDatos();
                    lblMensajeVaque.Text = "¡Solicitud de revisión exitosa!";
                }
                else
                {
                    lblMensajeVaque.Text = "¡Debes indicar los sintomas del animal!";
                }
            }
            else
            {
                lblMensajeVaque.Text = "¡Debes seleccionar al animal!";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (idSeleccionado.Text != "")
            {
                Random r = new Random();
                int random = r.Next(2, 4);
                int ConversionPeso = Convert.ToInt16(txtPeso.Text);
                int PesoAumentado = ConversionPeso * random;
                txtPeso.Text = Convert.ToString(PesoAumentado);
                lblMensajeVaque.Text = "¡Pesaje del animal exitoso!";
            }
            else
            {
                lblMensajeVaque.Text = "¡Debes seleccionar al animal!";
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (idSeleccionado.Text != "")
            {
                vaquero.GuardarPesaje(txtPeso.Text, txtRaza.Text, txtPrecio.Text, txtEdad.Text, txtSexo.Text, txtNombre.Text, Convert.ToInt32(idSeleccionado.Text));
                limpiarDatos();
                dtgvVaquero.DataSource = null;
                dtgvVaquero.DataSource = bovino.CargarBovino("");
                lblMensajeVaque.Text = "¡Pesaje guardado!";
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (idSeleccionado.Text != "")
            {
                int pesoEntero = Convert.ToInt16(txtPeso.Text);
                if (pesoEntero >= 500)
                {
                    if (txtDescripVaquero.Text != "")
                    {
                        vaquero.VentaBovino(txtPeso.Text, txtRaza.Text, txtPrecio.Text, txtEdad.Text, txtSexo.Text, txtNombre.Text, txtDescripVaquero.Text, idSeleccionado.Text);
                        limpiarDatos();
                        lblMensajeVaque.Text = "¡Animal puesto en venta exitosamente!";
                    }
                    else
                    {
                        lblMensajeVaque.Text = "¡Coloca una descripción del animal!";
                    }

                }
                else
                {
                    lblMensajeVaque.Text = "¡Este animal no cumple con el peso requerido!";
                }
            }
            else
            {
                lblMensajeVaque.Text = "¡Debes seleccionar al animal!";
            }
        }

        private void txtPeso_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRaza_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtPrecio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEdad_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void lbl_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Inicio inicio = new Inicio();
            inicio.Show(this);
            this.Hide();
        }
    }
}
