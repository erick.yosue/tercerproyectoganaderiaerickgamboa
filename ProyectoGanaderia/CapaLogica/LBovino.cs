﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using CapaDatos;
namespace CapaLogica
{
    public struct LBovino : InterfazCRUDBovinos
    {
        /*
         Este metodo es el encargado de registrar los bovinos
         Por parametro recibe toda la información del bovino que se desea registrar
         No retorna nada
             */
        public void RegistrarBovino(string peso, string raza, string precio, string edad, string sexo, string nombre)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                Bovino bovino = new Bovino();
                bovino.peso = peso;
                bovino.raza = raza;
                bovino.precio = precio;
                bovino.edad = edad;
                bovino.sexo = sexo;
                bovino.nombre = nombre;
                db.Bovino.Add(bovino);
                db.SaveChanges();
            }
        }
        /*
         Este método es el encargado de cargar los bovinos en un DataGV
         Por medio del parametro recibe un filtro de busqueda que es opcional su utilización
         Retorna una lista que contiene los bovinos que se cargaron
             */
        public List<EBovino> CargarBovino(string nombre)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                List<EBovino> lista = new List<EBovino>();
                var lst = from Bovino in db.Bovino
                          where Bovino.nombre.Contains(nombre)
                          select Bovino;
                foreach (var i in lst)
                {
                    EBovino bovi = new EBovino();
                    bovi.id = i.id;
                    bovi.peso = i.peso;
                    bovi.nombre = i.nombre;
                    bovi.sexo = i.sexo;
                    bovi.precio = i.precio;
                    bovi.edad = i.edad;
                    bovi.raza = i.raza;
                    lista.Add(bovi);
                }
                return lista;

            }

        }
        /*
         Este método se encarga de la edición del bovino
         Por parámetro recibe la información del bovino que se desea editar o sobrecargar
         No retorna nada
             */
        public void EditarBovino(string peso, string raza, string precio, string edad, string sexo, string nombre, int id)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                Bovino bovi = null;

                bovi = db.Bovino.Find(id);
                bovi.peso = peso;
                bovi.raza = raza;
                bovi.precio = precio;
                bovi.edad = edad;
                bovi.sexo = sexo;
                bovi.nombre = nombre;
                db.Entry(bovi).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();


            }
        }
        /*
         Este método es el encargado de eliminar el bovino que se desee
         Por parámetro recibe el id del bovino que se desea eliminar 
         No retorna nada
             */
        public void EliminarBovino(int id)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                Bovino bovi;
                bovi = db.Bovino.Find(id);
                db.Bovino.Remove(bovi);
                db.SaveChanges();
            }

        }
    }
}
