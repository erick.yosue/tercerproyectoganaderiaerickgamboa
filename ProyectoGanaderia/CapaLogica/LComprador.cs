﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;

namespace CapaLogica
{
    public class LComprador
    {
        /*
         Este método es el encargado de cargar los animales que estan en venta en un DataGV
         Por medio del parametro recibe un filtro de busqueda que es opcional su utilización
         Retorna una lista que contiene los bovinos en venta que se cargaron
             */
        public List<EComprador> CargarAnimalesVenta(string idanimal)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                List<EComprador> lista = new List<EComprador>();
                var lst = from VentaBovino in db.VentaBovino
                          where VentaBovino.idanimal.Contains(idanimal)
                          select VentaBovino;
                foreach (var i in lst)
                {
                    EComprador soli = new EComprador();
                    soli.id = i.id;
                    soli.idanimal = i.idanimal;
                    soli.peso = i.peso;
                    soli.raza = i.raza;
                    soli.precio = i.precio;
                    soli.edad = i.edad;
                    soli.sexo = i.sexo;
                    soli.nombre = i.nombre;
                    soli.descripcion = i.descripcion;
                    lista.Add(soli);
                }
                return lista;

            }

            


        }
        /*
         Este metodo es el encargado de registrar las compras de los bovinos
         Por parametro recibe toda la información del bovino comprado que se desea registrar
         No retorna nada
             */

        public void ComprarAnimal(string idAnimal, string finca)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                CarroCompras carrito = new CarroCompras();
                carrito.idanimal = idAnimal;
                carrito.finca = finca;
                db.CarroCompras.Add(carrito);
                db.SaveChanges();
            }
        }
        /*
         Este método es el encargado de cargar los animales comprados en un DataGV
         Por medio del parametro recibe un filtro de busqueda que es opcional su utilización
         Retorna una lista que contiene los bovinos comprados que se cargaron
             */
        public List<ECarritos> CargarAnimalescomprados(string finca)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                List<ECarritos> lista = new List<ECarritos>();
                var lst = from CarroCompras in db.CarroCompras
                          where CarroCompras.finca.Contains(finca)
                          select CarroCompras;
                foreach (var i in lst)
                {
                    ECarritos carri = new ECarritos();
                    carri.id = Convert.ToString(i.id);
                    carri.idanimal = i.idanimal;
                    carri.finca = i.finca;
                    lista.Add(carri);
                }
                return lista;

            }

        }
        /*
         Este método es el encargado de eliminar el bovino en venta porque ya se vendió 
         Por parámetro recibe el id del bovino que se desea eliminar 
         No retorna nada
             */
        public void QuitarAnimalVenta(int id)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                VentaBovino Vbovi;
                Vbovi = db.VentaBovino.Find(id);
                db.VentaBovino.Remove(Vbovi);
                db.SaveChanges();
            }

        }
    }


}
