﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;

namespace CapaLogica
{
    public class LVeterinario
    {

        /*
         Este método es el encargado de cargar las solicitudes de revisión en un DataGV
         Por medio del parametro recibe un filtro de busqueda que es opcional su utilización
         Retorna una lista que contiene las solicitudes que se cargaron
             */

        public List<ESolicitudRevision> CargarSolicitud(string raza)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                List<ESolicitudRevision> lista = new List<ESolicitudRevision>();
                var lst = from RevisionBovino in db.RevisionBovino
                          where RevisionBovino.raza.Contains(raza)
                          select RevisionBovino;
                foreach (var i in lst)
                {
                    ESolicitudRevision soli = new ESolicitudRevision();
                    soli.id = i.id;
                    soli.idAnimal = i.idanimal;
                    soli.peso = i.peso;
                    soli.raza = i.raza;
                    soli.precio = i.precio;
                    soli.edad = i.edad;
                    soli.sexo = i.sexo;
                    soli.nombre = i.nombre;
                    soli.descripcion = i.descripcion;
                    lista.Add(soli);
                }
                return lista;

            }
        }

        /*
        Este método es el encargado de eliminar la solicitud que se desee
        Por parámetro recibe el id de la solicitud que se desea eliminar 
        No retorna nada
            */

        public void EliminarSolicitud(int id)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                RevisionBovino Rebovi;
                Rebovi = db.RevisionBovino.Find(id);
                db.RevisionBovino.Remove(Rebovi);
                db.SaveChanges();
            }

        }
    }
}

