﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;

namespace CapaLogica
{
   public class LUsuario
    {
        /*
         Este metodo es el encargado de registrar los usuarios
         Por parametro recibe toda la información del usuario que se desea registrar
         No retorna nada
             */
        public void registrarUsuarios(string nombre, string contra, string cedula, string tipo)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                usuario usuario = new usuario();
                usuario.nombre = nombre;
                usuario.contrasena = contra;
                usuario.cedula = cedula;
                usuario.tipo = tipo;
                db.usuario.Add(usuario);
                db.SaveChanges();
            }

        }
    }
}
