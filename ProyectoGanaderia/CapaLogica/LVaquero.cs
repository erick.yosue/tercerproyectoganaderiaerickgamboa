﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;

namespace CapaLogica
{
    public class LVaquero
    {
        /*
         Este metodo es el encargado de registrar la solicitud de revisión de los bovinos
         Por parametro recibe toda la información de la solicitud que se desea registrar
         No retorna nada
             */
        public void SolicitudRevisionBovino(string peso, string raza, string precio, string edad, string sexo, string nombre, string descripcion, string idAnimal)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                RevisionBovino bovino = new RevisionBovino();
                bovino.idanimal = idAnimal;
                bovino.peso = peso;
                bovino.raza = raza;
                bovino.precio = precio;
                bovino.edad = edad;
                bovino.sexo = sexo;
                bovino.nombre = nombre;
                bovino.descripcion = descripcion;
                db.RevisionBovino.Add(bovino);
                db.SaveChanges();
            }
        }
        /*
        Este metodo es el encargado de guardar el pesaje de los bovinos
        Por parametro recibe toda la información del pesaje que se desea guardar
        No retorna nada
            */
        public void GuardarPesaje(string peso, string raza, string precio, string edad, string sexo, string nombre, int id)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                Bovino bovi = null;

                bovi = db.Bovino.Find(id);
                bovi.peso = peso;
                bovi.raza = raza;
                bovi.precio = precio;
                bovi.edad = edad;
                bovi.sexo = sexo;
                bovi.nombre = nombre;
                db.Entry(bovi).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();


            }
        }

        /*
        Este metodo es el encargado guardas los bovinosn que se desean vender
        Por parametro recibe toda la información del bovino que se desea guardar
        No retorna nada
            */

        public void VentaBovino(string peso, string raza, string precio, string edad, string sexo, string nombre, string descripcion, string idAnimal)
        {
            using (GanaderiaEntitiess db = new GanaderiaEntitiess())
            {
                VentaBovino bovino = new VentaBovino();
                bovino.idanimal = idAnimal;
                bovino.peso = peso;
                bovino.raza = raza;
                bovino.precio = precio;
                bovino.edad = edad;
                bovino.sexo = sexo;
                bovino.nombre = nombre;
                bovino.descripcion = descripcion;
                db.VentaBovino.Add(bovino);
                db.SaveChanges();
            }
        }


    }
}
