﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;

namespace CapaLogica
{
    interface InterfazCRUDBovinos
    {
        void RegistrarBovino(string peso, string raza, string precio, string edad, string sexo, string nombre);
        List<EBovino> CargarBovino(string nombre);
        void EditarBovino(string peso, string raza, string precio, string edad, string sexo, string nombre, int id);
        void EliminarBovino(int id);
    }
}
