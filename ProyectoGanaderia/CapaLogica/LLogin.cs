﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaLogica
    
{
   public class LLogin
    {
        /*
         Este método es en encargado de analizar los datos ingresados por el usuario en el incio de sesión
         y así identificar si los credenciales son o ne válidos, de serlos, también se encarga de indentificar
         a que tipo de usuario pertenece la cuenta.
         Por medio del parámetro recibe el usuario y la contraeña que se deben analizar
         Retorna un objeto de tipo usuario con su respectiva información
             */
        public EUsuario Login(string usuario, string contra)
        {
            EUsuario u = new EUsuario();
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = "DESKTOP-R9OGT38";
            builder.InitialCatalog = "Ganaderia";
            builder.IntegratedSecurity = true;
            var connectionString = builder.ToString();
            using (SqlConnection conexion = new SqlConnection(connectionString))
            {
                conexion.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM usuario WHERE cedula ='" + usuario + "' AND contrasena ='" + contra + "'", conexion))
                {
                    SqlDataAdapter data = new SqlDataAdapter(cmd);
                    DataTable res = new DataTable();
                    data.Fill(res);
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        u.id = Convert.ToInt32(res.Rows[0]["id"]);
                        u.Nombre = Convert.ToString(res.Rows[0]["nombre"]);
                        u.Contrasenna = Convert.ToString(res.Rows[0]["contrasena"]);
                        u.Cedula = Convert.ToString(res.Rows[0]["cedula"]);
                        u.tipo = Convert.ToString(res.Rows[0]["tipo"]);
                        dr.Close();
                        return u;
                    }
                    else
                    {
                        throw new Exception("Credenciales no válidos");
                    }
                }

            }
        }

    }
}
